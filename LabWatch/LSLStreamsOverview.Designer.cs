﻿namespace LabWatch
{
    partial class LSLStreamsOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.refresh_lsl_streams = new System.Windows.Forms.Button();
            this.streamTreeView = new LabWatch.MixedCheckBoxesTreeView();
            this.display = new GraphLib.PlotterDisplayEx();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.zoomInX = new System.Windows.Forms.Button();
            this.zoomOutX = new System.Windows.Forms.Button();
            this.zoomInY = new System.Windows.Forms.Button();
            this.zoomOutY = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.frequencyDisplay = new GraphLib.PlotterDisplayEx();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // refresh_lsl_streams
            // 
            this.refresh_lsl_streams.Location = new System.Drawing.Point(12, 12);
            this.refresh_lsl_streams.Name = "refresh_lsl_streams";
            this.refresh_lsl_streams.Size = new System.Drawing.Size(174, 46);
            this.refresh_lsl_streams.TabIndex = 0;
            this.refresh_lsl_streams.Text = "Refresh";
            this.refresh_lsl_streams.UseVisualStyleBackColor = true;
            this.refresh_lsl_streams.Click += new System.EventHandler(this.refresh_lsl_streams_Click);
            // 
            // streamTreeView
            // 
            this.streamTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.streamTreeView.Location = new System.Drawing.Point(0, 71);
            this.streamTreeView.Name = "streamTreeView";
            this.streamTreeView.Size = new System.Drawing.Size(452, 713);
            this.streamTreeView.TabIndex = 5;
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.Transparent;
            this.display.BackgroundColorBot = System.Drawing.Color.White;
            this.display.BackgroundColorTop = System.Drawing.Color.White;
            this.display.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.display.DashedGridColor = System.Drawing.Color.DarkGray;
            this.display.Dock = System.Windows.Forms.DockStyle.Fill;
            this.display.DoubleBuffering = true;
            this.display.Location = new System.Drawing.Point(0, 0);
            this.display.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.display.Name = "display";
            this.display.PlaySpeed = 0.5F;
            this.display.Size = new System.Drawing.Size(302, 787);
            this.display.SolidGridColor = System.Drawing.Color.DarkGray;
            this.display.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.zoomInX);
            this.splitContainer1.Panel1.Controls.Add(this.zoomOutX);
            this.splitContainer1.Panel1.Controls.Add(this.zoomInY);
            this.splitContainer1.Panel1.Controls.Add(this.zoomOutY);
            this.splitContainer1.Panel1.Controls.Add(this.streamTreeView);
            this.splitContainer1.Panel1.Controls.Add(this.refresh_lsl_streams);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1366, 787);
            this.splitContainer1.SplitterDistance = 455;
            this.splitContainer1.TabIndex = 7;
            // 
            // zoomInX
            // 
            this.zoomInX.Location = new System.Drawing.Point(356, 12);
            this.zoomInX.Name = "zoomInX";
            this.zoomInX.Size = new System.Drawing.Size(49, 46);
            this.zoomInX.TabIndex = 9;
            this.zoomInX.Text = "+ X";
            this.zoomInX.UseVisualStyleBackColor = true;
            this.zoomInX.Click += new System.EventHandler(this.zoomInX_Click);
            // 
            // zoomOutX
            // 
            this.zoomOutX.Location = new System.Drawing.Point(301, 12);
            this.zoomOutX.Name = "zoomOutX";
            this.zoomOutX.Size = new System.Drawing.Size(49, 46);
            this.zoomOutX.TabIndex = 8;
            this.zoomOutX.Text = "- X";
            this.zoomOutX.UseVisualStyleBackColor = true;
            this.zoomOutX.Click += new System.EventHandler(this.zoomOutX_Click);
            // 
            // zoomInY
            // 
            this.zoomInY.Location = new System.Drawing.Point(246, 12);
            this.zoomInY.Name = "zoomInY";
            this.zoomInY.Size = new System.Drawing.Size(49, 46);
            this.zoomInY.TabIndex = 7;
            this.zoomInY.Text = "+ Y";
            this.zoomInY.UseVisualStyleBackColor = true;
            // 
            // zoomOutY
            // 
            this.zoomOutY.Location = new System.Drawing.Point(192, 12);
            this.zoomOutY.Name = "zoomOutY";
            this.zoomOutY.Size = new System.Drawing.Size(48, 46);
            this.zoomOutY.TabIndex = 6;
            this.zoomOutY.Text = "- Y";
            this.zoomOutY.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.display);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.frequencyDisplay);
            this.splitContainer2.Size = new System.Drawing.Size(907, 787);
            this.splitContainer2.SplitterDistance = 302;
            this.splitContainer2.TabIndex = 5;
            // 
            // frequencyDisplay
            // 
            this.frequencyDisplay.BackColor = System.Drawing.Color.Transparent;
            this.frequencyDisplay.BackgroundColorBot = System.Drawing.Color.White;
            this.frequencyDisplay.BackgroundColorTop = System.Drawing.Color.White;
            this.frequencyDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.frequencyDisplay.DashedGridColor = System.Drawing.Color.DarkGray;
            this.frequencyDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frequencyDisplay.DoubleBuffering = true;
            this.frequencyDisplay.Location = new System.Drawing.Point(0, 0);
            this.frequencyDisplay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.frequencyDisplay.Name = "frequencyDisplay";
            this.frequencyDisplay.PlaySpeed = 0.5F;
            this.frequencyDisplay.Size = new System.Drawing.Size(601, 787);
            this.frequencyDisplay.SolidGridColor = System.Drawing.Color.DarkGray;
            this.frequencyDisplay.TabIndex = 5;
            // 
            // LSLStreamsOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 787);
            this.Controls.Add(this.splitContainer1);
            this.Name = "LSLStreamsOverview";
            this.Text = "LSLStreamsOverview";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button refresh_lsl_streams;
        private GraphLib.PlotterDisplayEx display;
        private MixedCheckBoxesTreeView streamTreeView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button zoomInX;
        private System.Windows.Forms.Button zoomOutX;
        private System.Windows.Forms.Button zoomInY;
        private System.Windows.Forms.Button zoomOutY;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private GraphLib.PlotterDisplayEx frequencyDisplay;
    }
}