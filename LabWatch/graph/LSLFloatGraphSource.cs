﻿using GraphLib;
using LabWatch.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWatch.graph
{
    public class LSLFloatGraphSource : LSLNummericGraphSource
    {
        public LSLFloatGraphSource(LSL.liblsl.StreamInfo cStream) 
            : base(cStream)
        {
            for (int i = 0; i < channelCount; i++)
            {
                DataSource timeDomainDatasource = new DataSource();
                timeDomainDatasource.SetDisplayRangeY(-1, 1);
                timeDomainDataSources.Add(timeDomainDatasource);
                DataSource frequencyDomainDataSource = new DataSource();
                frequencyDomainDataSource.SetDisplayRangeY(0, 1);
                frequencyDomainDataSources.Add(frequencyDomainDataSource);
            }


            ffTrans = new FFT2();
        }

        private uint _nyquist;
        private uint fftLength;

        private uint nyquistFrequency {
            get
            {
                return _nyquist;
            }
            set
            {
                if (_nyquist != value)
                {
                    _nyquist = value;
                    fftLength = _nyquist * 2;
                    ffTrans.init(fftLength);
                }
            }
        }
        FFT2 ffTrans;


        public override void refreshFrequencyDomainData(uint nyquist)
        {
            nyquistFrequency = nyquist;

            dynamic[][] realFFTLists = bufferedLSLstream.GetFromLast(fftLength);

            if (realFFTLists == null)
                return;

            int channelIndex = -1;
            foreach (DataSource ds in frequencyDomainDataSources)
            {

                channelIndex++;

                dynamic[] dynamicRealFFTList = realFFTLists[channelIndex];

                double[] realFFTList = new double[dynamicRealFFTList.Length];
                for (int i = 0; i < dynamicRealFFTList.Length; i++)
                {
                    realFFTList[i] = dynamicRealFFTList[i];
                }

                double[] imFFTList = new double[fftLength];
                double[] magnitude = new double[fftLength];
                double[] frequency = new double[fftLength];

                ffTrans.run(realFFTList, imFFTList, false);

                for (int i2 = 0; i2 < fftLength; i2++)
                {
                    magnitude[i2] = Math.Sqrt(realFFTList[i2] * realFFTList[i2] + imFFTList[i2] * imFFTList[i2]);

                    //frequency
                    frequency[i2] = i2 * (44100 / fftLength);
                }


                List<cPoint> samples = new List<cPoint>();

                int i3 = 0;
                for (i3 = 0; i3 < fftLength; i3++)
                {

                    if (frequency[i3] < (fftLength / 2))
                    {
                        cPoint p = new cPoint();
                        p.x = (float)frequency[i3];
                        p.y = (float)magnitude[i3];
                        
                        samples.Add(p);
                    }
                    // if samples is null its not added to the display object... gets not renedered at all
                 

                }
                ds.Samples = samples.ToArray<cPoint>();
            }

        }

        public override void refreshTimeDomainData(double fromTimestamp, double toTimestamp, int samplesInTimespan, int samplerate)
        {

            if (!this.bufferedLSLstream.subscripe)
                return;

            double[] timestamps;
            dynamic[][] samples = bufferedLSLstream.GetBetween(fromTimestamp, toTimestamp, samplerate, out timestamps);


            int samplesResolved = samples[0].Length;



            int channelIndex = -1;
            foreach (DataSource ds in timeDomainDataSources)
            {

                channelIndex++;
                if (ds.Samples == null)
                {
                    continue;
                }
                int i = 0;
                for (i = 0; i < samplesResolved && i < ds.Samples.Length; i++)
                {
                    // if samples is null its not added to the display object... gets not renedered at all
                    ds.Samples[i].x = i;
                    ds.Samples[i].y = samples[channelIndex][i] == null ? 0 : samples[channelIndex][i];

                }
                for (; i < ds.Samples.Length; i++)
                {
                    ds.Samples[i].x = i;
                }

            }
        }
    }
}
