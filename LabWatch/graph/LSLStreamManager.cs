﻿using LSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWatch.graph
{
    public class LSLStreamManager
    {
        public List<LSLGraphSource> streams = new List<LSLGraphSource>();

        /// <summary>
        /// Reads all available Sources from the Network and refreshes the List
        /// </summary>
        public bool refreshStreamList()
        {

            bool listChangedFlag = false;

            // request available Streams from lsl
            liblsl.StreamInfo[] availableStreams = liblsl.resolve_streams();

            // set streams offline that are no longer present
            foreach (LSLGraphSource stream in streams) // Loop through streams
            {
                bool streamOnlineFalg = false;

                for (int i = 0; i < availableStreams.Length; i++)
                {
                    liblsl.StreamInfo cStream = availableStreams[i];

                    if (stream.sourceId.Equals(cStream.source_id()))
                    {
                        streamOnlineFalg = true;
                    }
                }

                if (stream.online != streamOnlineFalg)
                {
                    listChangedFlag = true;
                    stream.online = streamOnlineFalg;
                }

            }

            // add streams that are not in the list yet
            for (int i = 0; i < availableStreams.Length; i++)
            {
                liblsl.StreamInfo cStream = availableStreams[i];

                bool listedFlag = false;

                foreach (LSLGraphSource stream in streams)
                {
                    if (stream.sourceId.Equals(cStream.source_id()))
                    {
                        listedFlag = true;
                    }
                }
                if (!listedFlag)
                {
                    liblsl.channel_format_t format = cStream.channel_format();
                    // for the beginning we choose the Graphsource Type by the channelformat...
                    // later we might use other indicators...
                    switch (format)
                    {
                        case LSL.liblsl.channel_format_t.cf_float32:
                            streams.Add(new LSLFloatGraphSource(cStream));

                            break;
                        case LSL.liblsl.channel_format_t.cf_string:
                            streams.Add(new LSLMarkerGraphSource(cStream));
                            break;
                        default:
                            // TODO implement other formats
                            throw new NotImplementedException("No GraphSource class for (" + format + ")  implemented");
                            break;
                    } 

                    listChangedFlag = true;
                }
            }
            return listChangedFlag;
        }
    }
}
