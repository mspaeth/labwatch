﻿using GraphLib;
using LabWatch.lsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWatch.graph
{
    public abstract class LSLGraphSource
    {

        public LSL.liblsl.StreamInfo lslStream;
        public double samplerate;
        public int channelCount;
        public string sourceId;

        public BufferedStream bufferedLSLstream;
        public List<DataSource> frequencyDomainDataSources = new List<DataSource>();
        public List<DataSource> timeDomainDataSources = new List<DataSource>();

        public bool online
        {
            get {
                return this.bufferedLSLstream.online;
            }
            set
            {
                this.bufferedLSLstream.online = value;
            }
        }

        public LSLGraphSource(LSL.liblsl.StreamInfo cStream)
        {
            // TODO: Complete member initialization
            this.lslStream = cStream;
            this.sourceId = this.lslStream.source_id();
            this.samplerate = this.lslStream.nominal_srate();
            this.channelCount = this.lslStream.channel_count();
            this.bufferedLSLstream = new BufferedStream(cStream);
            
            
        }

        public abstract void refreshTimeDomainData(double fromTimestamp, double toTimestamp, int samplesInTimespan, int samplerate);
        
    }
}
