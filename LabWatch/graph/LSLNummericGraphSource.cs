﻿using GraphLib;
using LabWatch.lsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWatch.graph
{
    public abstract class LSLNummericGraphSource : LSLGraphSource
    {

        public List<DataSource> frequencyDomainDataSources = new List<DataSource>();

        public LSLNummericGraphSource(LSL.liblsl.StreamInfo cStream)
            : base(cStream)
        {   
        }

        public abstract void refreshFrequencyDomainData(uint nyquist);
        
    }
}
