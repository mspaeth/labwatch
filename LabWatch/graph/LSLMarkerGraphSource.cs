﻿using GraphLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWatch.graph
{
    public class LSLMarkerGraphSource : LSLGraphSource
    {
        public LSLMarkerGraphSource(LSL.liblsl.StreamInfo cStream)
            : base(cStream)
        {
            // do we get multiple channels for markerstreams? I guess not...
            // for (int i = 0; i < channelCount; i++)
            //{
                timeDomainDataSources.Add(new DataSource());
            //}
        }

        private List<string> stringIntRepresentation = new List<string>();

        public override void refreshTimeDomainData(double fromTimestamp, double toTimestamp, int samplesInTimespan, int samplerate)
        {

            if (!this.bufferedLSLstream.subscripe || samplesInTimespan == 0)
                return;

            double[] timestamps;
            dynamic[][] samples = bufferedLSLstream.GetBetween(fromTimestamp, toTimestamp, samplerate, out timestamps);

            int samplesResolved = samples[0].Length;


            double sampleInterval = (toTimestamp - fromTimestamp) / samplesInTimespan;
            double currentTimestamp = fromTimestamp;

            int channelIndex = -1;
            foreach (DataSource ds in timeDomainDataSources)
            {

                channelIndex++;
                if (ds.Samples == null)
                {
                    continue;
                }
                int sampleIndex = 0;
                int i = 0;
                for (i = 0; i < ds.Samples.Length; i++)
                {
                    // if samples is null its not added to the display object... gets not renedered at all

                    ds.Samples[i].x = i;

                    while (sampleIndex < timestamps.Length && timestamps[sampleIndex] < currentTimestamp)
                    {
                        sampleIndex++;
                        // Console.WriteLine("sample skipped" + sampleIndex);
                            
                    }

                    if (sampleIndex < timestamps.Length && timestamps[sampleIndex] >= currentTimestamp && timestamps[sampleIndex] < (currentTimestamp + sampleInterval))
                    {
                       // Console.WriteLine("sample found for " + i + " samples that should be found " + samplesInTimespan + " current timestamp: " + currentTimestamp + " fromtimestanp " + fromTimestamp + " toTimestamp " + toTimestamp + " compared " + (currentTimestamp > toTimestamp));
                        
                        if (!stringIntRepresentation.Contains(samples[channelIndex][sampleIndex]))
                        {
                           // Console.WriteLine("string representation (" + samples[channelIndex][sampleIndex] + ") added");
                            stringIntRepresentation.Add(samples[channelIndex][sampleIndex]);
                        }

                        ds.Samples[i].y = ((float)stringIntRepresentation.IndexOf(samples[channelIndex][sampleIndex])) / 10;
                        ds.Samples[i].label = samples[channelIndex][sampleIndex];
                    }
                    else
                    {
                        ds.Samples[i].y = 0;
                        ds.Samples[i].label = "";
                    }
                    currentTimestamp = currentTimestamp + sampleInterval;
                }
                

            }
        }
    }
}
