﻿using LSL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabWatch.lsl
{
    public class BufferedStream
    {
        protected volatile bool _shouldStop = false;
        protected Thread dataFetchThread;

        protected liblsl.StreamInfo lslStream;
        protected bool _subscripe;

        protected int chunkSize = 1;
        protected liblsl.StreamInlet lslInlet;

        public double sampleRate;

        public int bufferForSeconds = 5;

        public StreamBuffer streamBuffer;

        private int timestampLookupCacheSize = 100;
        // public Dictionary<double, int> timestampPositionCache = new Dictionary<double, int>();

        public string sourceId
        {
            private set;
            get;
        }

        public int channelCount
        {
            set;
            get;
        }

        public bool Regular
        {
            get
            {
                return sampleRate > 0; 
            }
        }

        /// <summary>
        /// Flag indicates the status of the stream. Refreshed by the StreamManager 
        /// </summary>
        public bool online
        {
            set;
            get;
        }

        public bool subscripe
        {
            set
            {
                if (this._subscripe != value)
                {
                    if (value)
                    {
                        dataFetchThread = new Thread(this.StartFetching);
                        dataFetchThread.Start();
                        _subscripe = true;
                    }
                    else
                    {
                        this.RequestStop();
                        _subscripe = false;
                        // TODO destroy channel lists AND stop data collection Thread!
                    }
                }
            }
            get
            {
                return _subscripe;
            }
        }


        public BufferedStream(LSL.liblsl.StreamInfo streamInfo)
        {
            // TODO: Complete member initialization
            this.lslStream = streamInfo;
            this.sourceId = this.lslStream.source_id();
            this.sampleRate = this.lslStream.nominal_srate();
            this.channelCount = this.lslStream.channel_count();

            int capacity = Regular ? ((int)this.sampleRate) * bufferForSeconds : 5000;
            
            // TODO to reconsider for triggers and so on
           
            lslInlet = new liblsl.StreamInlet(lslStream);

            streamBuffer = new StreamBuffer(capacity, channelCount);

            //initializeStream();
        }

        public dynamic[][] GetFromLast(uint fftLength)
        {
            return this.streamBuffer.GetFromLast(fftLength);
        }

        public dynamic[][] GetBetween(double startTimestamp, double endTimestamp, int samplerate, out double[] timestamps)
        {
            int stepsize = Regular ? (int)this.sampleRate / samplerate : 1;
            return this.streamBuffer.GetBetween(startTimestamp, endTimestamp, stepsize, out timestamps);
        }

        //public abstract void initializeStream();

        public void StartFetching()
        {
            this._shouldStop = false;
            LSL.liblsl.channel_format_t format = lslStream.channel_format();
            // parentReference.lslStream.channel_format();
            switch (format)
            {
                case LSL.liblsl.channel_format_t.cf_float32:
                    fetchFloatData();
                    break;
                case LSL.liblsl.channel_format_t.cf_string:
                    fetchStringData();
                    break;
                default:
                    // TODO implement other formats
                    throw new NotImplementedException("Format ("+format.ToString() +") not implemented");
            }
            
        }

        private void fetchFloatData()
        {
            double[] timestamps = new double[chunkSize];
            float[,] fBuffer = new float[chunkSize, channelCount];
            while (!_shouldStop)
            {
                int num = this.lslInlet.pull_chunk(fBuffer, timestamps);
                this.streamBuffer.Add(timestamps, fBuffer, num);
            }
        }

        private void fetchStringData() {

            double[] timestamps = new double[1];
            while (!_shouldStop)
            {

                string[] sSamples = new string[channelCount];
                timestamps[0] = this.lslInlet.pull_sample(sSamples);
                //int num = this.lslInlet.pull_chunk(sBuffer, timestamps);
                string[,] sBuffer = new string[1, channelCount];
                for (int i = 0; i < channelCount; i++)
                {
                    sBuffer[0, i] = sSamples[i];
                }
                this.streamBuffer.Add(timestamps, sBuffer, 1);
            }
        }

        public void RequestStop()
        {
            _shouldStop = true;
        }

        

    }
}
