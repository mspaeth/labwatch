﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LabWatch.lsl
{
    public class StreamBuffer
    {
        protected int capacity;
        protected int size;
        protected int last;
        protected int first;
        protected int channelCount;

        protected double[] timestamps;
        protected dynamic[,] buffer;

        public StreamBuffer(int _capacity, int _channelCount)
        {
            capacity = _capacity;
            channelCount = _channelCount;
            this.Clear();
        }

        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        
        public int Last
        {
            get { return last; }
        }

        public int Size
        {
            get { return size; }
        }

        public void Clear()
        {
            buffer = new dynamic[channelCount, capacity];
            timestamps = new double[capacity];
            size = 0;
            last = -1;
            first = -1;
        }

        public virtual void Add(double[] _timestamps, dynamic samples, int sampleCount)
        {
            cacheLock.EnterWriteLock();
            for(int cSample = 0; cSample < sampleCount; cSample++) {

                

                last = (last + 1) % capacity;
                if (size < capacity)
                {
                    size++;
                    first = 0;
                }
                else
                {
                    first = (last + 1) % capacity; 
                } 

                timestamps[last] = _timestamps[cSample];
                for (int i = 0; i < channelCount; i++)
                {
                    // Console.WriteLine("added at " + last + " content " + samples[cSample, i]);
                    buffer[i,last] = samples[cSample, i];
                } 
            }

        
            
            cacheLock.ExitWriteLock();
        }

        public virtual void Add(double timestamp, dynamic[] item)
        {
            cacheLock.EnterWriteLock();

            last = (last + 1) % capacity;

            if (size < capacity)
            {
                size++;
                first = 0;
            }
            else
            {
                first = (last + 1) % capacity;
            }

            for (int i = 0; i <= item.Length; i++)
            {
                buffer[i,last] = item[i];
            }
            timestamps[last] = timestamp;


            cacheLock.ExitWriteLock();
        }

        public dynamic[] GetSamplesAtIndex(int i)
        {
            dynamic[] samplesBetween = new dynamic[channelCount];
            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                samplesBetween[channelIndex] = buffer[channelIndex,i];
            }
            return samplesBetween;
        }

        public dynamic[][] GetFromLast(uint samples)
        {
            cacheLock.EnterReadLock();

            dynamic[][] samplesBetween = new dynamic[channelCount][];
            if (this.size == 0 || this.size < samples)
            {

                cacheLock.ExitReadLock();
                return null;
            }



            int startIndex = (int) (this.last - samples);
            //Console.WriteLine(startIndex);
            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                samplesBetween[channelIndex] = GetSamplesByChannel(channelIndex, startIndex, (int)samples, 1);
            }

            cacheLock.ExitReadLock();

            return samplesBetween;
        }

        public dynamic[][] GetBetween(double startTimestamp, double endTimestamp, int stepsize, out double[] timestamps)
        {
            cacheLock.EnterReadLock();

            int startIndex = getFirstIndexAfter(startTimestamp);
            int endIndex = getLastIndexBefore(endTimestamp);

            //Console.WriteLine(startIndex+ " "  +endIndex);


            int nSamplesBetween = startIndex < endIndex ? (endIndex - startIndex) + 1 : capacity - startIndex + endIndex;


            dynamic[][] samplesBetween = new dynamic[channelCount][];
            

            if (startIndex == -1 || endIndex == -1)
            {
                for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
                {
                    samplesBetween[channelIndex] = new dynamic[0];
                }

                cacheLock.ExitReadLock();
                timestamps = new double[0];
                return samplesBetween;
            }

            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                samplesBetween[channelIndex] = GetSamplesByChannel(channelIndex, startIndex, nSamplesBetween, stepsize);
            }


            timestamps = GetTimestamps(startIndex, nSamplesBetween, stepsize);

            cacheLock.ExitReadLock();

            return samplesBetween;
        }

        public double[] GetTimestamps(int start, int n, int stepsize)
        {
            int timeStampsToExtract = n / stepsize;
            double[] timestampsToExtract = new double[timeStampsToExtract];
            for (int i = 0; i < timeStampsToExtract; i++, start = start + stepsize)
            {
                timestampsToExtract[i] = timestamps[(start % size)];
            }
            return timestampsToExtract;
        }

        int nfmod(int a, int b)
        {
            return (int)(a - b * Math.Floor((float)a / (float)b));
        }

        public dynamic[] GetSamplesByChannel(int channel, int start, int n, int stepsize)
        {
            int samplesToExtract = n / stepsize;
            dynamic[] channelSamples = new dynamic[samplesToExtract];
            for (int i = 0; i < samplesToExtract; i++, start = start + stepsize)
            {
                channelSamples[i] = buffer[channel, nfmod(start, size)];
            }
            return channelSamples;
        }

        public int getFirstIndexAfter(double timestamp)
        {
            cacheLock.EnterReadLock();

            // TODO optimize algorithm if needed
            for (int i = first; i <= first + (this.size -1); i++)
            {
                int index = i % this.size;
                if (timestamp < timestamps[index])
                {
                    cacheLock.ExitReadLock();
                    return index;
                }
            }

            cacheLock.ExitReadLock();
            return -1;
        }

        public int getLastIndexBefore(double timestamp)
        {
            cacheLock.EnterReadLock();

            // TODO optimize algorithm if needed
            for (int i = first + (this.size - 1); i >= first; i--)
            {
                int index = i % this.size;
                if (timestamp > timestamps[index])
                {
                    cacheLock.ExitReadLock();
                    return index;
                }
            }

            cacheLock.ExitReadLock();
            return -1;
        }

    }
}
