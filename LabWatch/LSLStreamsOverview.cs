﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LSL;
using GraphLib;
using LabWatch.graph;

namespace LabWatch
{
    public partial class LSLStreamsOverview : Form
    {

        private PrecisionTimer.Timer timer = null;
        private DateTime lastTimerTick = DateTime.Now;

        // graph properties
        private int pointsToPlot = 1000;
        private int _secondsToPlot;

        double plotStartTimestamp = -1;
        DateTime plotStartDateTime = DateTime.Now;

        DateTime lastReset = DateTime.Now;

        public int secondsToPlot {
            get
            {
                return _secondsToPlot;
            }
            set
            {
                _secondsToPlot = value;

                plotStartTimestamp = -1;
                //this.display.SetGridDistanceX(this.samplePointsToPlot / _secondsToPlot);
            } 
        }

        public LSLStreamsOverview()
        {
            InitializeComponent();
            streamTreeView.CheckBoxes = true;
            streamTreeView.AfterCheck += new TreeViewEventHandler(streamTreeView_OnCheckStateChange);
            streamManager = new LSLStreamManager();

            timer = new PrecisionTimer.Timer();
            timer.Period = 40;                         // 20 fps
            timer.Tick += new EventHandler(OnTimerTick);
            lastTimerTick = DateTime.Now;
            timer.Start();
            this.secondsToPlot = 2;
 
        }

        private void streamTreeView_OnCheckStateChange(object sender, TreeViewEventArgs e)
        {
            // TODO implement draw chart logic
            int index = e.Node.Index;

            if (e.Action != TreeViewAction.Unknown)
            {

                if (e.Node.Parent != null && e.Node.Parent.Tag is LSLGraphSource)
                {

                    bool childActive = false;
                    foreach (TreeNode child in e.Node.Parent.Nodes)
                    {
                        if (child.Checked)
                        {
                            childActive = true;
                            break;
                        }
                    }
                    ((LSLGraphSource)e.Node.Parent.Tag).bufferedLSLstream.subscripe = childActive;

 
                    bindTimeDomainDataSources();
                    bindTFrequencyDomainDataSources();
                }
                

            }
        }

        private void RefreshGraph()
        {
            frequencyDisplay.Refresh();
            display.Refresh();
        }


        private void OnTimerTick(object sender, EventArgs e)
        {
            int samplerate = this.pointsToPlot / secondsToPlot;
            

            //Console.WriteLine("this.pointsToPlot " + this.pointsToPlot + " secondsToPlot " +secondsToPlot+ " samplerate"+ samplerate);
            try
            {
                TimeSpan ts = DateTime.Now - lastReset;

                //int millisSinceZero = thisTimerTick.Subtract(plotXZero.value).Milliseconds;

                //int pointsToPlot = (int) (ts.TotalMilliseconds*((double)this.samplePointsToPlot / ((double)this.secondsToPlot * 1000)));

                //Console.WriteLine("PointsToPlot: " + pointsToPlot);
                // Console.WriteLine(liblsl.local_clock());
                if (plotStartTimestamp == -1 || ts.TotalMilliseconds > ((this.secondsToPlot * 1000)+1))
                {
                    //Console.WriteLine("restart Graph");
                    lastReset = DateTime.Now;
                    // Console.WriteLine("Start at 0");
                    plotStartTimestamp = liblsl.local_clock();
                    plotStartDateTime = DateTime.Now;
                }
                double plotToTimestamp = liblsl.local_clock();
                DateTime plotToDateTime = DateTime.Now;

                int samples = (int)((plotToDateTime - plotStartDateTime).TotalMilliseconds * (((float)samplerate) / 1000));

                foreach(LSLGraphSource stream in this.streamManager.streams) {
                    stream.refreshTimeDomainData(plotStartTimestamp, plotToTimestamp, samples, samplerate);
                    if (stream is LSLFloatGraphSource)
                    {
                        // TODO set frequncy depending on settings
                        ((LSLFloatGraphSource)stream).refreshFrequencyDomainData(4000);
                    }
                }
               
                this.Invoke(new MethodInvoker(RefreshGraph));
            }
            catch (ObjectDisposedException ex)
            {
                // we get this on closing of form
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            timer.Stop();
            timer.Dispose();
            base.OnClosed(e);
        }

        private void refresh_lsl_streams_Click(object sender, EventArgs e)
        {
            /*// wait until an EEG stream shows up
            liblsl.StreamInfo[] results = liblsl.resolve_stream("type", "Markers");

            // open an inlet and print meta-data
            liblsl.StreamInlet inlet = new liblsl.StreamInlet(results[0]);
            System.Console.Write(inlet.info().as_xml());

            // read samples
            string[,] sample = new string[1,50];
            double[] ts = new double[50];
            while (true)
            {
                inlet.pull_chunk(sample, ts);
                System.Console.WriteLine(sample[0,1]);
            }*/

            if (streamManager.refreshStreamList())
            {
                refresh_StreamsTree();
            }
        }


        private void refresh_StreamsTree()
        {
            this.SuspendLayout();
            streamTreeView.BeginUpdate();
            
            TreeNode streamNode;
            streamTreeView.Nodes.Clear();

            foreach (LSLGraphSource cStream in this.streamManager.streams)
            {
                streamNode = new HiddenCheckBoxTreeNode(cStream.sourceId);
                streamTreeView.Nodes.Add(streamNode);
                streamNode.Tag = cStream;
                //if (streamNode.Checked != cStream.subscripe)
                //    streamNode.Checked = cStream.subscripe;

                TreeNode channelNode;
                int i = 0;
                foreach (DataSource ds in cStream.timeDomainDataSources)
                {
                    channelNode = streamNode.Nodes.Add("Channel " + (i + 1));
                    channelNode.Tag = i;
                    i++;
                }
                /*int i2 = 0;
                foreach (DataSource ds in cStream.timeDomainDataSources)
                {
                    channelNode = streamNode.Nodes.Add("Channel " + (i2 + 1) + "Fq");
                    channelNode.Tag = i2;
                    i2++;
                }*/
               /* if (cStream is LSLStreamBufferFloat) // && ((LSLStreamFloat)cStream).subscripe && ((LSLStreamFloat)cStream).sampleBuffer != null)
                {
                    LSLStreamFloat fs = (LSLStreamBufferFloat)cStream;

                    int channelNr = 0;
                    foreach (LSLChannelBuffer<float> cb in fs.sampleBuffer)
                    {

                        channelNr++;

                        channelNode = streamNode.Nodes.Add("Channel " + (channelNr));
                        channelNode.Tag = cb;
                       
                    }
                } else if (cStream is LSLStreamString)// && ((LSLStreamFloat)cStream).subscripe && ((LSLStreamFloat)cStream).sampleBuffer != null)
                {
                    LSLStreamString fs = (LSLStreamString)cStream;

                    int channelNr = 0;
                    foreach (LSLChannelBuffer<string> cb in fs.sampleBuffer)
                    {

                        channelNr++;

                        channelNode = streamNode.Nodes.Add("Channel " + (channelNr));
                        channelNode.Tag = cb;

                    }
                }
                */
               
            }

            streamTreeView.EndUpdate();
            this.ResumeLayout();

        }

        void bindTFrequencyDomainDataSources()
        {
            this.SuspendLayout();

            frequencyDisplay.DataSources.Clear();


            foreach (TreeNode streamNode in this.streamTreeView.Nodes)
            {
                int channelIndex = 0;
                foreach (TreeNode channelNode in streamNode.Nodes)
                {
                    if (channelNode.Checked && streamNode.Tag is LSLNummericGraphSource)
                    {
                        DataSource channelDataSource = ((LSLNummericGraphSource)streamNode.Tag).frequencyDomainDataSources[channelIndex];
                        // todo move all this to LSL GraphSource? 
                        // channelDataSource.SetDisplayRangeY(0.05F, 0.9F);

                        Console.WriteLine(channelIndex + " " + channelNode.Text);
                        channelDataSource.Name = channelNode.Text;
                        channelDataSource.Length = pointsToPlot;
                        channelDataSource.AutoScaleX = true;
                        // TODO check overlapping Y 
                        channelDataSource.AutoScaleY = true;
                        Color[] cols = { Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0),
                                             Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0) ,
                                             Color.FromArgb(0,255,0),                              
                                             Color.FromArgb(0,255,0) };

                        channelDataSource.GraphColor = cols[0 % 7];
                        channelDataSource.OnRenderYAxisLabel = RenderYLabel;
                        //channelDataSource.OnRenderXAxisLabel += RenderXLabel;
                        frequencyDisplay.DataSources.Add(channelDataSource);
                    }
                    channelIndex++;
                }
            }
            frequencyDisplay.PanelLayout = PlotterGraphPaneEx.LayoutMode.STACKED;


            frequencyDisplay.BackgroundColorTop = Color.FromArgb(0, 64, 0);
            frequencyDisplay.BackgroundColorBot = Color.FromArgb(0, 64, 0);
            frequencyDisplay.SolidGridColor = Color.FromArgb(0, 128, 0);
            frequencyDisplay.DashedGridColor = Color.FromArgb(0, 128, 0);



            this.ResumeLayout();

            frequencyDisplay.Refresh();

        }

        

        void bindTimeDomainDataSources()
        {
            this.SuspendLayout();

            display.DataSources.Clear();

            
            foreach (TreeNode streamNode in this.streamTreeView.Nodes)
            {
                int channelIndex = 0;
                foreach(TreeNode channelNode in streamNode.Nodes) {
                    if (channelNode.Checked)
                    {
                        DataSource channelDataSource = ((LSLGraphSource)streamNode.Tag).timeDomainDataSources[channelIndex];
                        // todo move all this to LSL GraphSource? 
                        // channelDataSource.SetDisplayRangeY(0.05F, 0.9F);
                        
                        Console.WriteLine(channelIndex + " " + channelNode.Text);
                        channelDataSource.Name = channelNode.Text;
                        channelDataSource.Length = pointsToPlot;
                        channelDataSource.AutoScaleX = true;
                        // TODO check overlapping Y 
                        channelDataSource.AutoScaleY = false;
                        Color[] cols = { Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0),
                                             Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0), 
                                             Color.FromArgb(0,255,0) ,
                                             Color.FromArgb(0,255,0),                              
                                             Color.FromArgb(0,255,0) };

                        channelDataSource.GraphColor = cols[0 % 7];
                        channelDataSource.OnRenderYAxisLabel = RenderYLabel;
                        channelDataSource.OnRenderXAxisLabel += RenderXLabel;
                        display.DataSources.Add(channelDataSource);
                    }
                    channelIndex++;
                }
            }
            display.PanelLayout = PlotterGraphPaneEx.LayoutMode.STACKED;


            display.BackgroundColorTop = Color.FromArgb(0, 64, 0);
            display.BackgroundColorBot = Color.FromArgb(0, 64, 0);
            display.SolidGridColor = Color.FromArgb(0, 128, 0);
            display.DashedGridColor = Color.FromArgb(0, 128, 0);



            this.ResumeLayout();

            display.Refresh();
          
        }


        // TODO check posibilty to integrate markers here... 
        private String RenderXLabel(DataSource s, int idx)
        {
            if (s.AutoScaleX)
            {

                //Console.WriteLine(idx); 

                //if (idx % 2 == 0)
                {
                    return " " + s.Samples[idx].x;
                    // haha dirty trick after a bunch of beers to get seconds out fo scaling ;-)
                    int Value = idx ==  0 ? 0 : (int)Math.Round((((double)idx /(double) this.pointsToPlot) * secondsToPlot));
                    //Console.WriteLine(idx + " - " + Value);
                    return "" + Value + " ' ";
                }
                return "";
            }
            else
            {
                int Value = (int)(s.Samples[idx].x / 200);
                String Label = "" + Value + "\"";
                return Label;
            }
        }

        private String RenderYLabel(DataSource s, float value)
        {
            return String.Format("{0:0.0}", value);
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int index = e.Index;
            // TODO impement in streamnanager streamManager.streams[index].subscripe = (e.NewValue == CheckState.Checked);
        }

        private void zoomOutX_Click(object sender, EventArgs e)
        {
            this.secondsToPlot++;
        }

        private void zoomInX_Click(object sender, EventArgs e)
        {
            if (this.secondsToPlot > 1)

                this.secondsToPlot--;
        }

        public LSLStreamManager streamManager { get; set; }
    }
}
